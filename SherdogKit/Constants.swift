//
//  Constants.swift
//  SherdogKit
//
//  Created by Hjalti Jakobsson on 1.6.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation

struct SherdogKit {
    struct URLs {
        static let BaseURLString = "http://www.sherdog.com"
        static let EventsURLString = "http://www.sherdog.com/organizations/Ultimate-Fighting-Championship-2"
    }
}

public enum FightResult {
    case Win
    case Loss
    case Draw
}