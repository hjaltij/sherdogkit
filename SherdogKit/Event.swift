//
//  Event.swift
//  SherdogKit
//
//  Created by Hjalti Jakobsson on 31.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation

public typealias Result = ([Event], NSError?) -> Void

public class Event : Printable {
    
    public var title: String
    public var subtitle: String
    public var date: NSDate
    public var url: NSURL?
    public var fights: [Fight] = []
    
    private class var formatter: NSDateFormatter {
        struct Static {
            static let instance : NSDateFormatter = {
                let formatter = NSDateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                return formatter
                }()
        }
        
        return Static.instance
    }
    
    public var description: String {
        return "\(self.title) \(self.subtitle) : \(self.date)"
    }
    
    public init(title: String, subtitle: String, date: NSDate, url: NSURL?) {
        self.title = title
        self.subtitle = subtitle
        self.date = date
        self.url = url
    }
    
    //MARK: Event methods
    
    public class func allEvents(completion: Result) {
        
        var events: [Event] = []
        
        let urlSession = NSURLSession.sharedSession()
        let task = urlSession.dataTaskWithURL(NSURL(string:SherdogKit.URLs.EventsURLString)!) { data, _, error in
         
            if error != nil {
                
                Async.main {
                    completion([], error)
                }
                
                return
            }
            
            let doc = XMLDocument(data: data, documentType: .HTML, encoding: NSUTF8StringEncoding)
            let elements = doc?.selectElements("//table[contains(@class, 'event')]")
            var date = NSDate()
            
            for e in elements! {
                
                if let k = e.selectElements("tr/td") {
                    
                    for x in k {
                        
                        if let metaDate = x.selectFirstElement("meta") {
                            
                            if let dateString = metaDate.valueForAttribute("content"), formattedDate = self.formatter.dateFromString(dateString) {
                                date = formattedDate
                            }
                        }
                        
                        if let a = x.selectFirstElement("a"), content = a.content {
                            
                            var eventURL: NSURL?
                            
                            if let url = a.valueForAttribute("href") {
                                let baseURL = NSURL(string: SherdogKit.URLs.BaseURLString)!
                                eventURL = baseURL.URLByAppendingPathComponent(url)
                            }
                            
                            let stripped = content.trimmed()
                            let titles = self.parseTitle(stripped)
                            
                            events.append(Event(title: titles.title, subtitle: titles.subtitle, date: date, url: eventURL))
                        }
                        
                    }
                }
            }
            
            Async.main {
                completion(events, nil)
            }
        }
        
        task.resume()
    }
    
    public class func upcomingEvents(completion: Result) {
        filteredEvents(completion) { event in
            return event.date.compare(NSDate())  == NSComparisonResult.OrderedDescending
        }
    }
    
    public class func pastEvents(completion: Result) {
        filteredEvents(completion) { event in
            return event.date.compare(NSDate())  == NSComparisonResult.OrderedAscending
        }
    }
    
    private class func filteredEvents(completion: Result, filter: ((Event) -> Bool)) {
    
        allEvents { (events, error) in
            if error == nil {
                let filtered = events.filter(filter)
                completion(filtered, nil)
            }
            else {
                completion([], error)
            }
        }
    }
    
    public func loadEventInfo(completion: (Event, NSError?) -> Void) {
        
        self.fights.removeAll(keepCapacity: true)
        
        if let url = self.url {
            
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(url) { data, _, error in
                
                if let doc = XMLDocument(data: data, documentType: .HTML, encoding: NSUTF8StringEncoding) {
                    
                    var matchNumber = 0
                    
                    // Parse card
                    
                    if let matchNumberElements = doc.selectElements("//tr[contains(@itemtype, 'http://schema.org/Event')]/td[1]") {
                        
                        if let fighters1 = doc.selectElements("//td[contains(@class, 'text_right')]"), let fighters2 =  doc.selectElements("//td[contains(@class, 'text_left')]") {
                            
                            let fights = zip(Array(fighters1.generate()), Array(fighters2.generate()))
                            let fightsAndNumbers = zip(Array(matchNumberElements.generate()), fights)
                            
                            for el in fightsAndNumbers {
                                
                                var matchNumber = 0
                                
                                if let matchNumberString = el.0.content?.trimmed().toInt() {
                                    matchNumber = matchNumberString
                                }
                                
                                let f1 = Fighter(element: el.1.0)
                                let f2 = Fighter(element: el.1.1)
                                let fight = Fight(fighter1: f1, fighter2: f2, matchNumber: matchNumber)
                                
                                let fighter1El = el.1.0
                                
                                switch(self.fightResult(fighter1El))
                                {
                                case .Win:
                                    fight.winner = f1
                                case .Loss:
                                    fight.winner = f2
                                default:
                                    fight.winner = nil
                                }
                                
                                self.fights.append(fight)
                            }
                        }
                    }
                    
                    // Parse main event
                    
                    self.parseMainEvent(doc)
                }
                
                Async.main {
                    completion(self, nil)
                }
            }
            
            task.resume()
        }
    }
    
    //MARK: Private Methods
    
    private func fightResult(element: XMLElement) -> FightResult {
        
        var result = FightResult.Draw
        
        if let resultString = element.selectFirstElement(".//span[contains(@class, 'final_result')]")?.content {
            
            switch resultString {
            case "win":
                result = FightResult.Win
            case "loss":
                result = FightResult.Loss
            default:
                result = FightResult.Draw
            }
        }
        
        return result
    }
    
    private func parseMainEvent(doc: XMLDocument) {
        
        if let fighter1 = doc.selectFirstElement("//div[contains(@class, 'fighter left_side')]") {
            
            let mf1 = Fighter(mainEventElement: fighter1)
            
            if let fighter2 = doc.selectFirstElement("//div[contains(@class, 'fighter right_side')]") {
                let mf2 = Fighter(mainEventElement: fighter2)
                let maxMatchNumber =  self.fights.map { $0.matchNumber }.reduce(Int.min) { max($0, $1) }
                let mainFight = Fight(fighter1: mf1, fighter2: mf2, matchNumber: (maxMatchNumber + 1))
                
                switch(fightResult(fighter1))
                {
                case .Win:
                    mainFight.winner = mf1
                case .Loss:
                    mainFight.winner = mf2
                default:
                    mainFight.winner = nil
                }
                
                self.fights.insert(mainFight, atIndex: 0)
            }
        }
        
    }
    
    private class func parseTitle(title: String) -> (title: String, subtitle: String) {
        let titles = title.componentsSeparatedByString("-")
        
        if count(titles) > 1 {
            return (title: titles[0].trimmed(), subtitle: titles[1].trimmed())
        }
        
        return (title: title, subtitle: "")
    }
    
}