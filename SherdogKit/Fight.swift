//
//  Fight.swift
//  SherdogKit
//
//  Created by Hjalti Jakobsson on 1.6.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation

public class Fight : Printable {
    
    public var fighter1: Fighter
    public var fighter2: Fighter
    public var matchNumber = 0
    public var winner: Fighter?
    public var method: String?
    public var event: String?
    
    public var description: String {
        return "#\(matchNumber): \(fighter1.name) vs \(fighter2.name) W: \(winner?.name)"
    }
    
    init(fighter1: Fighter, fighter2: Fighter, matchNumber: Int) {
        self.fighter1 = fighter1
        self.fighter2 = fighter2
        self.matchNumber = matchNumber
    }
}