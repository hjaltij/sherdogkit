//
//  Fighter.swift
//  SherdogKit
//
//  Created by Hjalti Jakobsson on 1.6.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation

public class Fighter : Printable {
    
    public var name: String = ""
    public var url: NSURL?
    public var imageURL: NSURL?
    public var wins: Int = 0
    public var losses: Int = 0
    public var fights: [Fight] = []
    
    public var description: String {
        return "\(self.name) : \(self.url)"
    }
    
    init(name: String, url: NSURL?) {
        
        self.name = name
        self.url = url
    }
    
    init(element: XMLElement) {
        
        if let name = element.selectFirstElement(".//a/span[@itemprop='name']"), fighterName = name.content {
            self.name = fighterName.trimmed()
        }
        
        self.url = fighterURL(element)
        self.imageURL = imageURL(element)
    }
    
    init(mainEventElement: XMLElement) {
        
        if let imageURLEl = mainEventElement.selectFirstElement(".//img[@itemprop='image']"), imageURL = imageURLEl.valueForAttribute("src") {
            self.imageURL = NSURL(string: imageURL)
        }
        
        if let fighterElement = mainEventElement.selectFirstElement("h3/a") {
            
            if let urlString = fighterElement.valueForAttribute("href") {
                let baseURL = NSURL(string: SherdogKit.URLs.BaseURLString)!
                self.url = baseURL.URLByAppendingPathComponent(urlString)
            }
            
            if let name = fighterElement.content {
                self.name = name.trimmed()
            }
        }
    }
    
    public func loadImageURL(completion: () -> Void) {
     
        if let url = self.url {
            
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(url) { [unowned self] data, _, error in
                
                if let doc = XMLDocument(data: data, documentType: .HTML, encoding: NSUTF8StringEncoding) {
                    
                    if let fighterImageEl = doc.selectFirstElement("//img[@class='profile_image photo']") {
                        
                        if let urlString = fighterImageEl.valueForAttribute("src"), imgURL = NSURL(string: urlString) {
                            self.imageURL = imgURL
                        }
                    }
                }
                
                Async.main {
                    completion()
                }
            }
            
            task.resume()
        }
    }
    
    public func loadRecord(completion: (Fighter, NSError?) -> Void) {
        
        self.fights.removeAll(keepCapacity: true)
        
        if let url = self.url {
                        
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(url) { data, _, error in
             
                if let doc = XMLDocument(data: data, documentType: .HTML, encoding: NSUTF8StringEncoding) {
                    
                    if let fightElements = doc.selectElements("//tr[@class='odd' or @class='even']") {
                        
                        for fightEl in fightElements {
                            
                            var opponentName : String?
                            var opponentURL : NSURL?
                            
                            if let opponentElName = fightEl.selectFirstElement("./td[2]") {
                                opponentName = opponentElName.content
                            }
                            
                            if let opponentElURL = fightEl.selectFirstElement("./td/a") {
                                
                                if let opponentURLString = opponentElURL.valueForAttribute("href") {
                                    let baseURL = NSURL(string: SherdogKit.URLs.BaseURLString)!
                                    opponentURL = baseURL.URLByAppendingPathComponent(opponentURLString)
                                }
                            }
                            
                            if let name = opponentName, url = opponentURL {
                                let opponent = Fighter(name: name, url: url)
                                let fight = Fight(fighter1: self, fighter2: opponent, matchNumber: 0)
                                
                                if let methodEl = fightEl.selectFirstElement("./td[4]") {
                                    
                                    if let methodString = methodEl.firstChild?.content {
                                        fight.method = methodString
                                    }
                                }
                                
                                if let resultEl = fightEl.selectFirstElement("./td[1]"), resultString = resultEl.content {
                                    
                                    switch resultString {
                                    case "win":
                                        fight.winner = self
                                    case "loss":
                                        fight.winner = opponent
                                    default:
                                        fight.winner = nil
                                    }
                                    
                                }
                                
                                if let eventEl = fightEl.selectFirstElement("./td[3]/a"), event = eventEl.content {
                                    fight.event = event
                                }
                                
                                self.fights.append(fight)
                            }
                        }
                        
                        Async.main {
                            completion(self, nil)
                        }
                    }
                }
            }
            
            task.resume()
        }
    }
    
    private func fighterURL(element: XMLElement) -> NSURL? {
        
        if let name = element.selectFirstElement(".//a[@itemprop='url']"), fighterURL = name.valueForAttribute("href") {
            let baseURL = NSURL(string: SherdogKit.URLs.BaseURLString)!
            return baseURL.URLByAppendingPathComponent(fighterURL)
        }
        
        return nil
    }
    
    private func imageURL(element: XMLElement) -> NSURL? {
        
        if let imageURLEl = element.selectFirstElement(".//meta[@itemprop='image']"), imageURL = imageURLEl.valueForAttribute("content") {
            return NSURL(string: imageURL)
        }
        
        return nil
    }
    
}
