//
//  SherdogKit.h
//  SherdogKit
//
//  Created by Hjalti Jakobsson on 31.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

@import Foundation;

//! Project version number for SherdogKit.
FOUNDATION_EXPORT double SherdogKitVersionNumber;

//! Project version string for SherdogKit.
FOUNDATION_EXPORT const unsigned char SherdogKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SherdogKit/PublicHeader.h>


