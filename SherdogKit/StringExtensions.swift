//
//  StringExtensions.swift
//  SherdogKit
//
//  Created by Hjalti Jakobsson on 31.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation

extension String {
    
    func trimmed() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
}