//
//  SherdogKitTests.swift
//  SherdogKitTests
//
//  Created by Hjalti Jakobsson on 31.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import UIKit
import XCTest
import SherdogKit

class SherdogKitTests: XCTestCase {
    
    func testEvents() {
        
        let exp = self.expectationWithDescription("events")
        
        Event.allEvents { events, error in
         
            XCTAssert(events.count > 0, "Events array is empty")
            exp.fulfill()
        }


        self.waitForExpectationsWithTimeout(2.0, handler: nil)
    }
    
    func testEventParsing() {
        
        let exp = self.expectationWithDescription("basic event parsing")

        Event.allEvents { events, error in
        
            if let event = events.last {
                XCTAssert(count(event.title) > 0, "Title is empty")
                XCTAssert(count(event.subtitle) > 0, "Subtitle is empty")
                XCTAssertNotNil(event.date, "Date is empty")
                XCTAssertNotNil(event.url, "URL is empty")
            }
            
            exp.fulfill()
        }
        
        self.waitForExpectationsWithTimeout(2.0, handler: nil)
    }
    
    func testDetailedInfoParsing() {
        
        let exp = self.expectationWithDescription("detailed info parsing")
        
        Event.allEvents { events, error in
            
            let filteredEvents = events.filter { $0.url!.absoluteString! == "http://www.sherdog.com/events/UFC-Fight-Night-Hunt-vs-Bigfoot-32293" }
            
            if let event = filteredEvents.first {
                
                event.loadEventInfo { myEvent, error in
                    
                    XCTAssert(count(event.fights) > 0, "Events are empty")
                    
                    if let fight = myEvent.fights.first {
                        XCTAssertEqual(fight.matchNumber, 11, "Match number is not correct")
                        XCTAssertEqual(fight.fighter1.name, "Mark Hunt", "Name is not correct")
                        XCTAssertEqual(fight.fighter2.name, "Antonio Silva", "Name is not correct")
                        
                        XCTAssertNil(fight.winner, "Winner should not be set since this is a draw")
                    }
                    
                    let fight = myEvent.fights[4]
                    
                    XCTAssertEqual(fight.matchNumber, 7, "Match number is not correct")
                    XCTAssertEqual(fight.fighter1.name, "Clint Hester", "Name is not correct")
                    XCTAssertEqual(fight.fighter2.name, "Dylan Andrews", "Name is not correct")
                    XCTAssertEqual(fight.fighter1.url!.absoluteString!, "http://www.sherdog.com/fighter/Clint-Hester-43866", "URL is not correct")
                    XCTAssertEqual(fight.fighter2.url!.absoluteString!, "http://www.sherdog.com/fighter/Dylan-Andrews-21803", "URL is not correct")
                    
                    XCTAssertNotNil(fight.fighter1.imageURL, "Image URL is nil")
                    
                    if let imageURL = fight.fighter1.imageURL {
                        XCTAssertEqual(imageURL.absoluteString!, "http://www4.cdn.sherdog.com/image_crop/44/44/_images/fighter/1391302151491_20140107111155_clint_hester.JPG", "Image URL is incorrect")
                    }
                    
                    XCTAssertNotNil(fight.winner, "Winner is missing")
                    
                    if let winner = fight.winner {
                        XCTAssertEqual(winner.name, fight.fighter1.name, "Winner is incorrect")
                    }
                }
                
                exp.fulfill()
            }
        }
        
        self.waitForExpectationsWithTimeout(2.0, handler: nil)
    }
    
    func testEventInfoParsing() {
        
        let exp = self.expectationWithDescription("info parsing")
        
        Event.allEvents { events, error in
            
            if let event = events.last {
                
                event.loadEventInfo { event, error in
                    
                    XCTAssert(count(event.fights) > 0, "Events are empty")
                    
                    if let fight = event.fights.first {
                        XCTAssertEqual(fight.matchNumber, 8, "Match number is not correct")
                        XCTAssertEqual(fight.fighter1.name, "Royce Gracie", "Name is not correct")
                        XCTAssertEqual(fight.fighter2.name, "Gerard Gordeau", "Name is not correct")
                        
                        XCTAssertNotNil(fight.fighter1.imageURL, "Image URL is missing")
                        XCTAssertNotNil(fight.winner, "Winner is missing")
                        XCTAssertEqual(fight.fighter1.url!.absoluteString!, "http://www.sherdog.com/fighter/Royce-Gracie-19")
                        
                        if let winner = fight.winner {
                            XCTAssertEqual(winner.name, fight.fighter1.name, "Winner is incorrect")
                        }
                        
                        if let imageURL = fight.fighter1.imageURL {
                            XCTAssertEqual(imageURL.absoluteString!, "http://www4.cdn.sherdog.com/image_crop/72/72/_images/fighter/1406922560439_20140801124400_IMG_2688.JPG")
                        }
                    }
                    
                    if let fight = event.fights.last {
                        XCTAssertEqual(fight.matchNumber, 1, "Match number is not correct")
                        XCTAssertEqual(fight.fighter1.name, "Gerard Gordeau", "Name is not correct")
                        XCTAssertEqual(fight.fighter2.name, "Teila Tuli", "Name is not correct")
                        XCTAssertEqual(fight.fighter1.url!.absoluteString!, "http://www.sherdog.com/fighter/Gerard-Gordeau-15", "URL is not correct")
                        XCTAssertEqual(fight.fighter2.url!.absoluteString!, "http://www.sherdog.com/fighter/Teila-Tuli-16", "URL is not correct")
                        
                        XCTAssertNotNil(fight.fighter1.imageURL, "Image URL is nil")
                        
                        if let imageURL = fight.fighter1.imageURL {
                            XCTAssertEqual(imageURL.absoluteString!, "http://www1.cdn.sherdog.com/image_crop/44/44/_images/fighter/1406922695654_20130218022750_gerard_gordeau.JPG", "Image URL is incorrect")
                        }
                        
                        XCTAssertNotNil(fight.winner, "Winner is missing")
                        
                        if let winner = fight.winner {
                            XCTAssertEqual(winner.name, fight.fighter1.name, "Winner is incorrect")
                        }
                    }
                    
                    exp.fulfill()
                }
            }
        }
        
        self.waitForExpectationsWithTimeout(10.0, handler: nil)
    }
    
    func testUpcomingEvents() {
        
        let exp = self.expectationWithDescription("upcoming parsing")
        
        Event.upcomingEvents { upcoming, error in
        
            XCTAssert(upcoming.count > 0, "Upcoming events is empty")
            
            if let event = upcoming.last {
                
                XCTAssert(event.date.compare(NSDate()) == NSComparisonResult.OrderedDescending, "Date is not in the future")
                
            }
            
            exp.fulfill()
        }
        
        self.waitForExpectationsWithTimeout(2.0, handler: nil)
    }
    
    func testPastEvents() {
        
        let exp = self.expectationWithDescription("upcoming parsing")
        
        Event.pastEvents { pastEvents, error in
            
            XCTAssert(pastEvents.count > 0, "Past events is empty")
            
            if let event = pastEvents.last {
                
                XCTAssert(event.date.compare(NSDate()) == NSComparisonResult.OrderedAscending, "Date is not the past")
                
            }
            
            exp.fulfill()
        }
        
        self.waitForExpectationsWithTimeout(2.0, handler: nil)
    }
    
    func testFighterRecord() {
        
        let exp = self.expectationWithDescription("fighter parsing")
        
        Event.pastEvents { pastEvents, error in
            
            if let event = pastEvents.last {
                
                event.loadEventInfo { event, error in
                    
                    if let firstFight = event.fights.first {
                        
                        let fighter1 = firstFight.fighter1
                        fighter1.loadRecord { fighter, error in
                            
                            XCTAssert((fighter.fights.count > 0), "Fights are empty")
                            
                            if let fight = fighter.fights.first {
                               
                                XCTAssertEqual(fight.fighter2.name, "Kazushi Sakuraba", "Opponent name is incorrect")
                                XCTAssertNotNil(fight.winner, "Winner is not set")
                                XCTAssertEqual(fight.method!, "Decision (Unanimous)", "Win method is incorrect")
                                XCTAssertNotNil(fight.event, "Event is not set")
                                
                                if let event = fight.event {
                                    XCTAssertEqual(event, "K-1 HERO's - Dynamite!! USA")
                                }
                                
                                fight.fighter2.loadImageURL {
                                    XCTAssertNotNil(fight.fighter2.imageURL)
                                    XCTAssertEqual(fight.fighter2.imageURL!.absoluteString!, "http://www1.cdn.sherdog.com/image_crop/200/300/_images/fighter/20140820101831_IMG_2687.JPG", "Image URL for opponent is not correct")
                                    exp.fulfill()
                                }
                                
                                if let winner = fight.winner {
                                    XCTAssertEqual(winner.name, "Royce Gracie")
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        
        self.waitForExpectationsWithTimeout(20.0, handler: nil)
        
    }
}
